<?php

namespace App\Http\Controllers\Post;

use App\models\Post;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function posts(){
        $posts = Post::get();
        return view('posts.post',[
            'posts'=>$posts
        ]);
    }

    public function store(Request $request){

        $this->validate($request,[
            'text'=> ['required', 'max:100']
        ]);

        $request->user()->posts()->create([
            'text'=>$request->text
        ]);

        return back();
    }
}
