<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    public function index(){
        return view("authentication.register");
    }

    public function store(Request $request){
        $this->validate($request,[
            'firstname'=> ['required', 'max:50'],
            'lastname'=> ['required', 'max:50'],
            'email'=> ['required', 'email', 'max:50'],
            'password'=> ['required', 'confirmed']
        ]);
        
        User::create([
            'firstname'=> $request->firstname,
            'lastname'=> $request->lastname,
            'email'=> $request->email,
            'password'=> Hash::make($request->password)
        ]);
        
        auth()->attempt($request->only('email', 'password'));

        return redirect()->route('home');
    }
}
