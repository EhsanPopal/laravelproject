<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function signin(Request $request){
        $this->validate($request,[
            'email'=> ['required', 'email', 'max:50'],
            'password'=> ['required']
        ]);
        
        if(! auth()->attempt($request->only('email', 'password'))){
            return back()->with('status', 'Invalid login details');
        }

        return redirect()->route('home');   
    }

    public function index(){
        return view('authentication.login');
    }
}
