<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\Post\PostController;
use App\Http\Controllers\HomeControler;

// Route::get('/home', [HomeControler::class, 'home'])->name('home')->middleware('auth');

Route::get('/signout',[LogoutController::class, 'logout'])->name('logout');

Route::get('/signin',[LoginController::class, 'index'])->name('login');
Route::post('/signin',[LoginController::class, 'signin']);

Route::get('/signup',[RegisterController::class, 'index'])->name('register');
Route::post('/signup',[RegisterController::class, 'store']);

Route::get('/posts',[PostController::class, 'posts'])->name('posts');
Route::post('/posts',[PostController::class, 'store']);
