@extends('layouts.app')

@section('content')
    <div class="card p-3 mt-3 mb-3">
        <form action="{{ route('posts') }}" method="POST">
            @csrf
            @error('text')
                <small class="text-red">{{ $message }}</small>
            @enderror
            <textarea name="text" cols="30" rows="3" class="form-control" placeholder="write something here..." ></textarea>
            <input type="submit" value="Post" class="btn btn-primary mt-3">
        </form>
    </div>

    <div class="card">
        @if ($posts->count())
            @foreach ($posts as $post)
                <div class="p-2">
                    <div class="d-flex"><h3 class="post-author">{{ $post->user->firstname }} </h3><span class="post-date pt-1 pl-2">{{ $post->created_at->diffForHumans() }}</span></div>
                    <p class="post-text">{{ $post->text }}</p>
                    <hr class="post-divider">
                </div>
            @endforeach
            <p class="post-date m-0 p-2 text-center">click for more...</p>
        @else
            <p class="post-date m-0 p-2">there are no posts :(</p>
        @endif
    </div>
@endsection