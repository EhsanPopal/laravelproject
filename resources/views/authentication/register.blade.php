@extends('layouts.login')

@section('pageTitle')
    Sign up
@endsection

@section('pageContent')
    <div class="d-flex justify-content-center">
    <form action="{{ route('register') }}" method="POST" class="col-5 card p-3 mt-5">
            @csrf
            <h3 class="text-center pb-2 pt-5">Sign up</h3>
            @error('firstname')
                <small class="text-red">{{ $message }}</small>
            @enderror
            <input type="text" name="firstname" placeholder="first name" class="form-control mb-3" value="{{ old('firstname') }}">
            @error('lastname')
                <small class="text-red">{{ $message }}</small>
            @enderror
            <input type="text" name="lastname" placeholder="last name" class="form-control mb-3" value="{{ old('lastname') }}">
            @error('email')
                <small class="text-red">{{ $message }}</small>
            @enderror
            <input type="email" name="email" placeholder="email" class="form-control mb-3" value="{{ old('email') }}">
            @error('password')
                <small class="text-red">{{ $message }}</small>
            @enderror
            <input type="password" name="password" placeholder="password" class="mb-3 form-control">
            @error('password_confirmation')
                <small class="text-red">{{ $message }}</small>
            @enderror
            <input type="password" name="password_confirmation" placeholder="confirm passsword" class="mb-3 form-control">
            <input type="submit" value="Sign up" class="btn btn-primary">
            <small class="pt-2 text-center">Already have an account? <a href="#">Sign in here</a></small>
        </form>
    </div>
@endsection