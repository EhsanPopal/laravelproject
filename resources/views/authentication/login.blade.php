@extends('layouts.login')

@section('pageTitle')
    Sign in
@endsection

@section('pageContent')
    <div class="col-5 ml-auto mr-auto">
        @if (session('status'))
            <div class="error-msg">
                {{ session('status') }}
            </div>
        @endif
    </div>
    <div class="d-flex justify-content-center">
        <form action="{{ route('login') }}" method="POST" class="col-5 card p-3 mt-5">
            @csrf
            <h3 class="text-center pb-2 pt-5">Sign in</h3>
            @error('email')
                <small class="text-red">{{ $message }}</small>
            @enderror
            <input type="text" name="email" placeholder="email" class="form-control mb-3">
            @error('password')
                <small class="text-red">{{ $message }}</small>
            @enderror
            <input type="password" name="password" placeholder="password" class="mb-3 form-control">
            <input type="submit" value="Sign in" class="btn btn-primary">
        <small class="pt-2 text-center">Dont have an account? <a href="{{ route('register') }}">Register here</a></small>
        </form>
    </div>
@endsection