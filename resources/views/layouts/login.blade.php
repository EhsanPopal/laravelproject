<!DOCTYPE html>
<html lang="en">
<head>
    <link href="/css/app.css" rel="stylesheet">
    <title>@yield('pageTitle')</title>
</head>
<body class="grand-back">
    @yield('pageContent')
    <script src="/js/app.js"></script>
</body>
</html>