<!DOCTYPE html>
<html lang="en">
<head>
    <link href="/css/app.css" rel="stylesheet">
    <title>App</title>
</head>
<body class="grand-back">
    {{-- navbar --}}
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark justify-content-center">
        <div class="collapse navbar-collapse justify-content-between col-6" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home page <span class="sr-only">(current)</span></a>
                </li>
                @auth    
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Liked <span class="sr-only">(current)</span></a>
                    </li>
                @endauth
            </ul>
            <ul class="navbar-nav pl-5">
                @auth
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            More
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}">Logout</a>
                            <div class="dropdown-divider"></div>
                            <p class="dropdown-item m-0" href="#">{{auth()->user()->firstname}} {{auth()->user()->lastname}}</p>
                        </div>
                    </li>
                @endauth
                @guest
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('login') }}">Sign in <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('register') }}">Sign up <span class="sr-only">(current)</span></a>
                    </li>
                @endguest
            </ul>
        </div>
    </nav>

    {{-- content section --}}
    <section class="ml-auto mr-auto col-6">
        @yield('content')
    </section>

    <script src="/js/app.js"></script>
</body>
</html>